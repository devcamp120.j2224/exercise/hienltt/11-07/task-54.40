package com.devcamp.s50.task5440.restapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CPizzaCampaign {
    String username;
    String firstname;
    String lastname;

    public CPizzaCampaign(){
        this.username = "lam";
    }

    public CPizzaCampaign(String username){
        this.username = username;
    }

    @GetMapping("/devcamp/hello-pizza")
    public String hello(){             
        String newline = System.getProperty("line.separator"); // tạo break line
        CPizzaCampaign person = new CPizzaCampaign("Hienltt");
        int randomNumber = 1 + (int)(Math.random() * ((6 - 1) + 1));
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
    
        return String.format("Hello pizza lover! Hôm nay %s, mua 1 tặng 1.", dtfVietNam.format(today)) + newline + "Xin chào " + person.username + ". Số may mắn hôm nay của bạn là: " + randomNumber;
        
    }
}
